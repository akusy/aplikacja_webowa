# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Websocket::Application.config.secret_key_base = '867f5211f8103b01e98566778acdd7c0d763a3e92a7426c4e5e93349c6913b1cf275cd4ebdb43cd2a7990b1c10261ac1ca7a9ac76e8451629c9f449ec3d48417'

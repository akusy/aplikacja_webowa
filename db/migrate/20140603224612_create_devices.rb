class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.integer :user_id
      t.boolean :recording, default: false

      t.timestamps
    end
  end
end

class EventsController < WebsocketRails::BaseController

  def android
    broadcast_message :android, "message"
  end

  def android_stop
    broadcast_message :android_stop, "message"
  end


  def test
    new_message = {:message => 'odpowiedz'}
    broadcast_message :test, new_message
  end

  def index
    puts message
    videos = current_user.videos.reload.map{ |p| p.videofile.gsub("public/videos/", '') }.reverse

    if message[:last_video] 
      videos = videos.select{ |v| v > message[:last_video] }
    end
    new_message = {message: {videos: videos} }
    broadcast_message :test, new_message
  end

end

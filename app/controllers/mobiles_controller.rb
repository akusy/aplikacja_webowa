class MobilesController < ApplicationController
  skip_before_action :verify_authenticity_token

  before_action :authenticate_mobile_user, only: [:order, :video]

  def video

    if params["file"] && params["file"].tempfile.size > 6000

      filename = params["file"].original_filename #"#{Time.now.to_i}.3gp"
      directory = "public/videos/"
      path = File.join(directory, filename)
      File.open(path, "wb") { |f| f.write(params["file"].tempfile.read) }
      current_user.videos.create(videofile: path)

      render json: {}, status: 200
    else
      render json: {}, status: 400
    end
  end

  def login
    user = User.find_by(email: params[:email])
    auth_token = Devise.friendly_token
    if user && user.valid_password?(params[:password])
      user.update_attribute(:auth_token, auth_token)
      render json: {auth_token: auth_token}, status: 200
    else
      render json: {}, status: :unauthorized
    end

  end

  def order 
    if current_user.device.recording
      order = "record_start"
    else
      order = "record_stop"
    end
    render json: {order: order}, status: 200
  end

  def set_status
    p params
    render json: {order: ""}, status: 200
  end

  private

  def authenticate_mobile_user
    user = User.find_by(auth_token: params[:auth_token])

    if user
      @current_user = user
    else
      render json: {}, status: :unauthorized
    end
  end
end

class HomeController < ApplicationController
  before_action :authenticate_user!

  respond_to :json

  def index
    
  end

  def start_stop
    if params[:record] == "start"
      current_user.device.update_attributes(recording: true)
    elsif params[:record] == "stop"
      current_user.device.update_attributes(recording: false)
    end
    render json: {order: ""}, status: 200
  end


  private

end

class VideosController < ApplicationController
  respond_to :json

  def index
    @videos = current_user.videos
    render json: @videos
  end
end

$(document).ready ->
  if $('#video_recording_area').length > 0
    mediaConstraints =
      audio: true
      video: true

    onVideoFail = (e) ->
      console.log "webcam fail!", e
      return

    hasGetUserMedia = ->
      !!(navigator.getUserMedia or navigator.webkitGetUserMedia or navigator.mozGetUserMedia or navigator.msGetUserMedia)

    if hasGetUserMedia()
      "Good to go!"
    else
      alert "getUserMedia() is not supported in your browser"

    stopRecording = ->
      streamRecorder.getRecordedData postVideoToServer
      return

    postVideoToServer = (videoblob) ->
      console.log getTime()
      data = {}
      data.name = new Date().getTime()
      data.video = videoblob
      data.metadata = "test metadata"
      data.action = "upload_video"
      jQuery.post "http://localhost:3000/video", videoblob, onUploadSuccess
      return

    onUploadSuccess = ->
      alert "video uploaded"
      return

    onMediaSuccess = (stream) ->
      mediaRecorder = new MediaStreamRecorder(stream)
      mediaRecorder.mimeType = "video/webm"

      mediaRecorder.videoWidth = 320
      mediaRecorder.videoHeight = 240

      mediaRecorder.ondataavailable = (blob) ->
        uploadFiles "/video", blob
        return

      mediaRecorder.start 2000
      return
      
    uploadFiles = (url, file) ->
      fileType = "video" # or "audio"
      fileName = new Date().getTime() + ".webm" # or "wav" or "ogg"
      formData = new FormData()
      formData.append fileType + "-filename", fileName
      formData.append fileType + "-blob", file
      
      #for (var i = 0, file; file = files[i]; ++i) {
      #formData.append(file.name, file);
      #}
      xhr = new XMLHttpRequest()
      xhr.open "POST", url, true
      
      #xhr.onload = function(e) { ... };
      xhr.send formData # multipart/form-data
      return

    window.URL = window.URL or window.webkitURL
    navigator.getUserMedia = navigator.getUserMedia or navigator.webkitGetUserMedia or navigator.mozGetUserMedia or navigator.msGetUserMedia
    streamRecorder = undefined
    webcamstream = 0
    if navigator.getUserMedia
      navigator.getUserMedia
        audio: true
        video: true
      , ((stream) ->
        webcamstream = stream
        onMediaSuccess stream
        return
      ), onVideoFail
    else
      alert "failed"

  $("button#stop_rec").bind "click", ->
    console.log webcamstream
    webcamstream.stop()
    mediaRecorder.stop()
    webcamstream = undefined
    navigator = undefined
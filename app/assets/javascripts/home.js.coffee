# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->

#============================================= Włączanie i wyłącznie nagrywania
  $("#send_ws").click ->
    $.post "/start_stop", record: "start"
    #dispatcher.trigger "android", "comment"
  $("#send_stop_ws").click ->
    #dispatcher.trigger "android_stop", "comment"
    $.post "/start_stop", record: "stop"

  dispatcher = new WebSocketRails('localhost:3000/websocket')#WebSocketRails('50.117.7.162:3000/websocket')


  dispatcher.bind "android", (data) ->
    #console.log data

  if $("#webcamcontrols").length > 0

    dispatcher.trigger "test", {comment: "test"}

    videoPlayer = document.getElementById("homevideo")
    videos = []
    video_list = []
    inRun = false

    $('#stop').click ->
      stop()

    $('#play').click ->
      dispatcher.trigger("test", {}) if videos.length is 0
      playNextVideo() unless inRun

    dispatcher.bind "test", (data) ->
      if data.message.videos
        videos = data.message.videos.concat(videos)
        for v in data.message.videos.reverse()
          unless _.contains(video_list, v)
            video_list.unshift v
            recording_time = new Date(+v.match(/\d+/)[0]).toUTCString()
            $('ul.recordings').prepend("<li class='recording' data-id=#{v}><a>#{recording_time}</a></li>")

     
    $('ul.recordings').on "click", ".recording", ->
      videos = []
      dispatcher.trigger "test", {last_video: $(this).data('id') }
      videos.push($(this).data('id')) if videos.length is 0
      stop()
      playNextVideo()

    playNextVideo = ()->
      $('li').removeClass('played')
      nextVideo = videos.pop()
      if nextVideo
        $("#homevideo > source").attr("src", "videos/" + nextVideo)
        run() unless inRun
        $("li[data-id='#{nextVideo}']").addClass('played')

    dispatcher.on_close = (data) ->
      console.log "Connection has been closed: ", data

    run = ()->
      inRun = true
      if videos.length < 5
        dispatcher.trigger "test", {last_video: videos[0] }
      videoPlayer.load()
      videoPlayer.play()

    stop = ()->
      inRun = false
      videoPlayer.pause()

    videoPlayer.addEventListener 'ended', (e) ->
      if videos.length > 0 && inRun is true
        playNextVideo()
        run()
      else
        inRun = false
        dispatcher.trigger "test", {}

    setInterval (->
      unless inRun
        dispatcher.trigger("test", {last_video: _.first(video_list)})
        console.log "interwał"
    ), 3000
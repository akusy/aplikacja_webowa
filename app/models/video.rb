class Video < ActiveRecord::Base
  before_destroy :delete_video

  belongs_to :user

  def delete_video
    begin
      File.delete(self.videofile)
    rescue
      Rails.logger.error "Error: deleting file: #{self.videofile}"
    end
  end
end
